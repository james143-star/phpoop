<?php

class FormVerification{
    public $name;
    public $address;
    public $age;
    public $contact;

    public function examine(){
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $address = $_POST['address'];
            $age = $_POST['age'];
            $contact = $_POST['contact'];


            if(!empty($name) && (!empty($address)) && (!empty($age)) && (!empty($contact))){
                echo "<center>";
                echo "<div style='width: 30%; heigth: 50%; border: 1px solid black; padding:20px; margin-top: 50px;' >";
                echo "<h3><marquee>Informations</marquee></h3>";
                echo "<p>___________________________________________</p>";
                echo "<div style='margin-left: 10px;'>";
                echo "Username: ".$name;
                echo "<br>Address: ".$address;
                echo "<br>Age: ".$age;
                echo "<br>Contact Number: ".$contact;
                echo "</div>";
                echo "</div>";
                echo "</center>";
            } else{
                echo "<center>";
                echo "<div style='width: 50%; ' >";
                echo"<h1><marquee>Something forgotten to fill up...</marquee></h1>";
                echo "</div>";
                echo "</center>";
            }
        }
    }
} 
$requirements=new FormVerification();
$requirements->examine();       

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Exercise4</title>

    <style>
    body{
        background-color: #FF9A8B;
        background-image: linear-gradient(90deg, #FF9A8B 0%, #FF6A88 55%, #FF99AC 100%);    

    }
</style>
</head>

<body>

<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card my-3">
            <div class="card-header">
                <h2 class="text-center">Exercise 4</h2>
            </div>
            <div class="card-body bg-dark">
            <form action="" method="post">
        <label style="color: white" for="" class="form-label">Enter name</label>
        <input type="text" class="form-control" name="name">
        <label style="color: white" for="" class="form-label">Enter address</label>
        <input type="text" class="form-control" name="address">
        <label style="color: white" for="" class="form-label">Enter age</label>
        <input type="number" class="form-control" name="age">
        <label style="color: white" for="" class="form-label">Enter contact number</label>
        <input type="text" class="form-control" name="contact">
        <br>
        <br>
        <button style="width: 100%;" type="submit" class="btn btn-primary" name="submit">Submit</button>
    </form>
            </div>
        </div>
    </div>
    </div>
   </div>


    
</body>
</html>