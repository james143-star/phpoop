<?php
session_start();
class userInput
{
    public $userInput;

    public function __construct($userInput)
    {
        $this->userInput = $userInput;
    }

    public function addInput()
    {
        if (!isset($_SESSION['option'])) {
            $_SESSION['option'] = [];
        }
        array_push($_SESSION['option'], $this->userInput);
        echo '<center><select name="dropdown"></center>';
        foreach ($_SESSION['option'] as $value) {
            echo '<option style="width: 40%;" value=' . $value . '>' . $value . '</option>';
        }
        echo '</select>';
    }
}

if (isset($_POST['submit'])) {
    $userInput = $_POST['userInput'];
    $input = new userInput($userInput);
    $input->addInput();
}   
?>

<!DOCTYPE html>
<html lang="en">
<style>
    body{
        background-color: #FF9A8B;
        background-image: linear-gradient(90deg, #FF9A8B 0%, #FF6A88 55%, #FF99AC 100%);    

    }
</style>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Number 2</title>
</head>
<body>
   <div class="container">
    <div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card my-3">
            <div class="card-header">
                <h2 class="text-center">Exercise 2</h2>
            </div>
            <div class="card-body bg-dark">
            <form action="" method="post">
        <label style="color: white" for="" class="form-label">Enter something</label>
        <input type="text" class="form-control" name="userInput">
        <br>
        <br>
        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
    </form>
            </div>
        </div>
    </div>
    </div>
   </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>